//
//  JsonFileReader.swift
//  TabBarApplication
//
//  Created by Admin on 17.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation
import SwiftyJSON

class JsonFileReader{
    
    var fileName : String!
    var fileExtension : String!
    
    init(fileName : String, fileExtension : String){
        self.fileName = fileName
        self.fileExtension = fileExtension
    }
    
    func parceJsonFile() -> [Animal]! {
        
        var result = [Animal]()
        guard let path = Bundle.main.url(forResource: fileName, withExtension: fileExtension) else{
            return nil
        }
        
        let data = try! Data(contentsOf: path)
        
        let jsonData = JSON(data: data, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
        
        guard let animals = jsonData["animals"].array else{
            return nil
        }
        
        for animal in animals{
            let name = animal["name"]
            let image = animal["picture"]
            let description = animal["description"]
            
            let cellAnimal = Animal(name: name.description, picturePath: image.description, description: description.description)
            result.append(cellAnimal)
        }

        return result
    }
    
}
