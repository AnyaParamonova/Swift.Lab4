//
//  AnimalDetailedViewController.swift
//  TabBarApplication
//
//  Created by Admin on 17.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class AnimalDetailedViewController: UIViewController {

    @IBOutlet weak var animalImage: UIImageView!
    
    @IBOutlet weak var animalShortDescription: UILabel!
    
    @IBOutlet weak var animalLongDescription: UITextView!
    
    var animalDescriptionShort: String?
    var animalDescriptionLong: String?
    var animalPicture : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let animalDescriptionShort = animalDescriptionShort {
            animalShortDescription.text = animalDescriptionShort
        }
        
        if let animalDescriptionLong = animalDescriptionLong {
            animalLongDescription.text = animalDescriptionLong
        }
        
        if let animalPicture = animalPicture {
            animalImage.image = animalPicture
        }
    }
    
}
