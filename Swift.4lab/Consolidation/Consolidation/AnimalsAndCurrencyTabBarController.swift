//
//  AnimalsAndCurrencyTabBarController.swift
//  Consolidation
//
//  Created by Admin on 24.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class AnimalsAndCurrencyTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        allocateTabBar()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            self.allocateTabBar()
        })
    }

    private func allocateTabBar(){
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            self.tabBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.tabBar.frame.height)
        } else{
            self.tabBar.frame = CGRect(x: 0, y: self.view.frame.height - self.tabBar.frame.height, width: self.view.frame.width, height: self.tabBar.frame.height)
        }
    }
    
    func swipeAction(swipe: UISwipeGestureRecognizer){
        switch swipe.direction{
        case UISwipeGestureRecognizerDirection.left:
            self.selectedIndex += 1
        case UISwipeGestureRecognizerDirection.right:
            self.selectedIndex -= 1
        default:
            break
        }
    }

}
