//
//  ByteConverter.swift
//  SimpleConverter
//
//  Created by Admin on 13.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class ByteConverter{
    
    func convertValueInBytesToBits(valueInBytes: Double ) -> Double {
        let result = valueInBytes * 1024
        return roundTo(places: 2, value: result)
    }
    
    func convertValueInBytesToMegabytes (valueInBytes : Double) -> Double {
        let result = valueInBytes / (1024 * 1024)
        return roundTo(places: 2, value: result)
    }
    
    func convertValueInBytesToGigabytes (valueInBytes : Double) -> Double {
        let result = valueInBytes / (1024 * 1024 * 1024)
        return roundTo(places: 2, value: result)
    }
    
    private func roundTo(places : Int, value : Double) -> Double {
        let divisor = pow(10.0, Double(places))
        return (divisor * value).rounded() / divisor;
    }
}

